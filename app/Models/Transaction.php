<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'type',
        'total',
        'saldo_before',
        'saldo_after',
        'via',
        'status',
        'transfer_to',
        'transaction_code',
        'file',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transferTo()
    {
        return $this->belongsTo(User::class, 'transfer_to');
    }
}
