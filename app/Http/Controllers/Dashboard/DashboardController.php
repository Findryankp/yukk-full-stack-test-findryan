<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $transactions = Transaction::where("user_id",Auth::user()->id)
                    ->orderBy('created_at', 'desc')
                    ->take(5)
                    ->get();

        $date1 = "2024-".date('m')."-01";
        $date2 = "2024-".date('m')."-31";

        $inMonth = Transaction::where('user_id', Auth::user()->id)
        ->whereBetween('created_at', [$date1, $date2])
        ->sum('total');

        return view('dashboard.index', compact("transactions","inMonth"));
    }

    public function history()
    {
        $transactions = Transaction::where("user_id",Auth::user()->id)->get();
        return view('dashboard.history',compact("transactions"));
    }
}
