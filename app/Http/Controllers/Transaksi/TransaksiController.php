<?php

namespace App\Http\Controllers\Transaksi;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Alert;

class TransaksiController extends Controller
{
    public function index()
    {
        $transactions = Transaction::where("user_id",Auth::user()->id)->take(5)->get();
        return view('dashboard.index',compact("transactions"));
    }

    private function updateSaldo($userId, $amount)
    {
        $user = User::find($userId);

        if (!$user) {
            return false;
        }

        $user->saldo += $amount;
        $user->save();

        return true;
    }

    public function transfer()
    {
        $users = User::where('id','!=',Auth::user()->id)->get();
        return view('transaksi.transfer',compact('users'));
    }

    function generateEncryptedTransactionCode($userIdFrom, $userIdTo,$type) {
        $currentDateTime = now();
        $randomString = Str::random(10);

        $transactionCode = $userIdFrom . "-" . $userIdTo . "-" . $currentDateTime->format('YmdHis') . "-" . $randomString;

        // Encrypt the transaction code
        $encryptedTransactionCode = $type.Crypt::encryptString($transactionCode);

        return $encryptedTransactionCode;
    }

    public function transferStore(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);

        if (!$user || $request->amount > $user->saldo) {
            Alert::error('Sorry!', 'Your balance is not egough!');
            return redirect('transaksi/transfer');
        }

        $userTo = User::find($request->transfer_to);

        if (!$userTo) {
            Alert::error('Sorry!', 'User not found!');
            return redirect('transaksi/transfer');
        }

        $code = $this->generateEncryptedTransactionCode($userId,$userTo->id,"TRF_");

        // Use database transactions to ensure data consistency
        DB::transaction(function () use ($userId, $userTo, $request,$user,$code) {
            $this->updateSaldo($userId, $request->amount * -1);
            $this->updateSaldo($userTo->id, $request->amount);

            Transaction::create([
                'user_id'       => $userId,
                'type'          => 'transferTo',
                'total'         => $request->amount*-1,
                'saldo_before'  => $user->saldo,
                'saldo_after'   => $user->saldo - $request->amount,
                'status'        => 'success',
                'via'           => 'web',
                'transfer_to'   => $userTo->id,
                'transaction_code' => $code,
            ]);

            Transaction::create([
                'user_id'       => $userTo->id,
                'type'          => 'transferFrom',
                'total'         => $request->amount,
                'saldo_before'  => $userTo->saldo,
                'saldo_after'   => $userTo->saldo + $request->amount,
                'status'        => 'success',
                'via'           => 'web',
                'transfer_to'   => $userId,
                'transaction_code' => $code,
            ]);
        });

        Alert::success('Hore!', 'Transfer Successfully');
        return redirect('dashboard');
    }

    public function topup()
    {
        return view('transaksi.topup');
    }

    public function topupStore(Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric',
            'keterangan' => 'required|string',
            'file' => 'required|file|mimes:jpg,jpeg,png|max:12048',
        ]);

        //create to da
        $trx = Transaction::create([
            'user_id'       => Auth::user()->id,
            'type'          => 'topup',
            'total'         => $request->amount,
            'saldo_before'  => Auth::user()->saldo,
            'saldo_after'   => Auth::user()->saldo + $request->amount,
            'status'        => 'success',
            'via'           => 'web',
            'transfer_to'   => Auth::user()->id,
            'transaction_code' => $this->generateEncryptedTransactionCode(Auth::user()->id,Auth::user()->id,"TOP_"),
        ]);

        // Handle file upload
        $fileName = $trx->id . '.png';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file->storeAs('uploads', $fileName, 'public');
        }

        Transaction::where('id',$trx->id)->update([
            "file" => $fileName,
        ]);

        $this->updateSaldo(Auth::user()->id, $request->amount);

        Alert::success('Hore!', 'Top Up Successfully');

        return redirect('/dashboard')->with('success', 'Topup submitted successfully!');
    }
}
