# WalletTrack - YUKK
<div align="center">
  <a href="./logo.png">
    <img src="./logo.png" alt="Logo" width="25%">
  </a>
</div>

## 💫 About
WalletTrack adalah aplikasi manajemen keuangan yang memudahkan pengguna untuk melakukan registrasi, login, dan melacak semua transaksi keuangan mereka. Dengan WalletTrack, pengguna dapat mengelola keuangan mereka dengan lebih efisien dan melacak setiap perubahan dalam saldo mereka secara transparan

## 🚀 Features
Aplikasi ini dilengkapi dengan fitur-fitur berikut:

### Halaman Register & Login:
Pengguna dapat mendaftar dan masuk ke akun mereka.

### Halaman Transaksi:
* Pilihan untuk melakukan Top Up atau Transaksi.
* Input jumlah (amount) untuk transaksi.
* Input keterangan untuk menjelaskan transaksi.
* Untuk Top Up, pengguna perlu mengunggah bukti pembayaran.
* Setiap transaksi memiliki kode unik untuk pelacakan.

### Halaman Riwayat Saldo:
* Pagination untuk menavigasi riwayat transaksi dengan mudah.
* Pencarian berdasarkan kode transaksi atau keterangan transaksi.
* Filter untuk menyaring transaksi berdasarkan kategori (Top Up atau Transaksi).
* Menampilkan saldo aktual pengguna.

## 🎯 Run Project
```shell
php artisan serve
```

## 😎 Development by
[![Findryankp](https://img.shields.io/badge/Findryankp-grey?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Findryankp)
[![findryankp](https://img.shields.io/badge/findryankp-blue?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/Findryankp/)
