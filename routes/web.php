<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Transaksi\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(AuthController::class)->group(function() {
    Route::get('/register', 'register')->name('register');
    Route::post('/register', 'store')->name('store');
    Route::post('/login', 'authenticate')->name('authenticate');
    Route::post('/logout', 'logout')->name('logout');
});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::prefix('dashboard')->group(function () {
        Route::get('/',         		[DashboardController::class, 'index'])->name('home');
        Route::get('/history',          [DashboardController::class, 'history']);
    });

    Route::prefix('transaksi')->group(function () {
        Route::get('/transfer',         [TransaksiController::class, 'transfer']);
        Route::post('/transfer',        [TransaksiController::class, 'transferStore'])->name('transferStore');

        Route::get('/topup',            [TransaksiController::class, 'topup']);
        Route::post('/topup',           [TransaksiController::class, 'topupStore'])->name('topupStore');
    });
});
