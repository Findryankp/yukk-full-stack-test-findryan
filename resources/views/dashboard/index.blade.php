@extends('templates.layouts')

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="card">
        <div class="card-header bg-primary text-white">
            Top Up Balance
        </div>
        <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{url('http://localhost:8000/transaksi/topup')}}" class="btn btn-primary">Top Up Now</a>
        </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="card">
        <div class="card-header bg-success text-white">
            Transfer
        </div>
        <div class="card-body">
            <h5 class="card-title">Special title treatment</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="{{url('http://localhost:8000/transaksi/transfer')}}" class="btn btn-success">Transfer Now</a>
        </div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="row">
            <div class="col-lg-12">
            <!-- Yearly Breakup -->
            <div class="card overflow-hidden">
                <div class="card-body p-4">
                <h5 class="card-title mb-9 fw-semibold">Current Balance</h5>
                <div class="row align-items-center">
                    <div class="col-8">
                    <h4 class="fw-semibold mb-3">Rp. {{Auth()->user()->saldo}}</h4>
                    <div class="d-flex align-items-center mb-3">
                        <span
                        class="me-1 rounded-circle bg-light-success round-20 d-flex align-items-center justify-content-center">
                        <i class="ti ti-arrow-up-left text-success"></i>
                        </span>
                        <p class="text-dark me-1 fs-3 mb-0">+9%</p>
                        <p class="fs-3 mb-0">last year</p>
                    </div>
                    <div class="d-flex align-items-center">
                        <div class="me-4">
                        <span class="round-8 bg-primary rounded-circle me-2 d-inline-block"></span>
                        <span class="fs-2">2024</span>
                        </div>
                    </div>
                    </div>
                    <div class="col-4">
                    <div class="d-flex justify-content-center">
                        <div id="breakup"></div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="col-lg-12">
            <!-- Monthly Earnings -->
            <div class="card">
                <div class="card-body">
                <div class="row alig n-items-start">
                    <div class="col-8">
                    <h5 class="card-title mb-9 fw-semibold"> Monthly Earnings</h5>
                    <h4 class="fw-semibold mb-3">Rp. {{$inMonth}}</h4>
                    <div class="d-flex align-items-center pb-1">
                        <span
                        class="me-2 rounded-circle bg-light-danger round-20 d-flex align-items-center justify-content-center">
                        <i class="ti ti-arrow-down-right text-danger"></i>
                        </span>
                        <p class="text-dark me-1 fs-3 mb-0">+9%</p>
                        <p class="fs-3 mb-0">last year</p>
                    </div>
                    </div>
                    <div class="col-4">
                    <div class="d-flex justify-content-end">
                        <div
                        class="text-white bg-secondary rounded-circle p-6 d-flex align-items-center justify-content-center">
                        <i class="ti ti-currency-dollar fs-6"></i>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div id="earning"></div>
            </div>
            </div>
        </div>
    </div>

    <div class="col-lg-8 d-flex align-items-strech">
        <div class="card w-100">
            <div class="card-body p-4">
            <h5 class="card-title fw-semibold mb-4">Recent Transactions</h5>
            <div class="table-responsive">
                <table class="table text-nowrap mb-0 align-middle">
                <thead class="text-dark fs-4">
                    <tr>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Assigned</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Amount</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Status</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Date</h6>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $no => $t)
                    <tr>
                        <td class="border-bottom-0">
                            <h6 class="fw-semibold mb-1">Sunil Joshi</h6>
                            @if($t->type == "transferTo" || $t->type == "transferTo")
                                <span class="fw-normal">Transfer</span>
                            @else
                                <span class="fw-normal">Topup</span>
                            @endif
                        </td>
                        <td class="border-bottom-0">
                            <p class="mb-0 fw-normal">{{$t->total}}</p>
                        </td>
                        <td class="border-bottom-0">
                            <div class="d-flex align-items-center gap-2">
                            <span class="badge bg-success rounded-3 fw-semibold">
                                <i class="ti ti-check"></i>
                            </span>
                            </div>
                        </td>
                        <td class="border-bottom-0">
                            <h6 class="fw-semibold mb-0 fs-4">{{$t->created_at}}</h6>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection


