@extends('templates.layouts')

@section('content')
<div class="row">
    <div class="col-12 d-flex align-items-strech">
        <div class="card w-100">
            <div class="card-body p-4">
            <h5 class="card-title fw-semibold mb-4">History Transactions</h5>
            <div class="table-responsive">
                <table id="myTable" class="table text-nowrap mb-0 align-middle">
                <thead class="text-dark fs-4">
                    <tr>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">No</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Assigned</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Amount</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Status</h6>
                    </th>
                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Date</h6>
                    </th>

                    <th class="border-bottom-0">
                        <h6 class="fw-semibold mb-0">Detail</h6>
                    </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transactions as $no => $t)
                    <tr>
                        <td class="border-bottom-0">
                            <p class="mb-0 fw-normal">{{$no+1}}</p>
                        </td>
                        <td class="border-bottom-0">
                            <h6 class="fw-semibold mb-1">Sunil Joshi</h6>
                            @if($t->type == "transferTo" || $t->type == "transferTo")
                                <span class="fw-normal">Transfer</span>
                            @else
                                <span class="fw-normal">Topup</span>
                            @endif
                        </td>
                        <td class="border-bottom-0">
                            <p class="mb-0 fw-normal">{{$t->total}}</p>
                        </td>
                        <td class="border-bottom-0">
                            <div class="d-flex align-items-center gap-2">
                            <span class="badge bg-success rounded-3 fw-semibold">
                                <i class="ti ti-check"></i>
                            </span>
                            </div>
                        </td>
                        <td class="border-bottom-0">
                            <h6 class="fw-semibold mb-0 fs-4">{{$t->created_at}}</h6>
                        </td>
                        <td class="border-bottom-0">
                            <div datas="{{$t}}" class="d-flex align-items-center gap-2 data-detail" data-bs-toggle="modal" data-bs-target="#exampleModal">
                                <span class="badge bg-primary rounded-3 fw-semibold">
                                    <i class="ti ti-eye"></i>
                                </span>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-12 mb-3">
                <label class="form-label">Date</label>
                <input readonly id="date" type="text" class="form-control" >
            </div>
            <div class="col-6 mb-3">
                <label class="form-label">Type</label>
                <input readonly id="type" type="text" class="form-control" >
            </div>

            <div class="col-6 mb-3">
                <label class="form-label">Total</label>
                <input readonly id="total" type="text" class="form-control" >
            </div>

            <div class="col-6 mb-3">
                <label class="form-label">Balance Before</label>
                <input readonly id="balance1" type="text" class="form-control" >
            </div>

            <div class="col-6 mb-3">
                <label class="form-label">Balance After</label>
                <input readonly id="balance2" type="text" class="form-control" >
            </div>

            <div class="col-6 mb-3">
                <label class="form-label">Via</label>
                <input readonly id="via" type="text" class="form-control" >
            </div>

            <div class="col-6 mb-3">
                <label class="form-label">Status</label>
                <input readonly id="status" type="text" class="form-control" >
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
$(document).ready( function () {
    $('#myTable').DataTable();
});

$('.data-detail').click(function() {
    var attributeValue = $(this).attr('datas');
    var jsonObject = JSON.parse(attributeValue);

    $("#date").val(jsonObject.created_at)
    $("#type").val(jsonObject.type)
    $("#total").val(jsonObject.total)
    $("#balance1").val(jsonObject.saldo_before)
    $("#balance2").val(jsonObject.saldo_after)
    $("#via").val(jsonObject.via)
    $("#status").val(jsonObject.status)
});
</script>
@endsection


