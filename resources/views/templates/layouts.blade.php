<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>WalletTrack</title>
  <link rel="shortcut icon" type="image/png" href="{{asset('assets/assets/images/logos/favicon.png')}}" />
  <link rel="stylesheet" href="{{asset('assets/assets/css/styles.min.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/assets/css/jquery.dataTables.min.css')}}" />
</head>

<body>
    @include('sweetalert::alert')
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    @include('templates.sidebar')

    <!--  Main wrapper -->
    <div class="body-wrapper">
        @include('templates.header')
      <div class="container-fluid">
        <!--  Row 1 -->
        @yield('content')

        <div class="py-6 px-6 text-center">
          <p class="mb-0 fs-4">Design and Developed by <a href="https://www.linkedin.com/in/findryankp/" target="_blank" class="pe-1 text-primary text-decoration-underline">Findryan Kurnia Pradana</a></p>
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('assets/assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('assets/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('assets/assets/js/sidebarmenu.js')}}"></script>
  <script src="{{asset('assets/assets/js/app.min.js')}}"></script>
  <script src="{{asset('assets/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
  <script src="{{asset('assets/assets/libs/simplebar/dist/simplebar.js')}}"></script>
  <script src="{{asset('assets/assets/js/dashboard.js')}}"></script>
  <script src="{{asset('assets/assets/js/jquery.dataTables.min.js')}}"></script>
  @yield('js')
</body>

</html>
