<!-- Sidebar Start -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div>
    <div class="brand-logo d-flex align-items-center justify-content-between">
        <a href="./index.html" class="text-nowrap logo-img">
            <h1 class="mt-2">WalletTrack</h1>
        </a>
        <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
        <i class="ti ti-x fs-8"></i>
        </div>
    </div>
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav scroll-sidebar" data-simplebar="">
        <ul id="sidebarnav">
        <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Home</span>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="{{url('dashboard')}}" aria-expanded="false">
            <span>
                <i class="ti ti-layout-dashboard"></i>
            </span>
            <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">Menu</span>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="{{url('transaksi/transfer')}}" aria-expanded="false">
            <span>
                <i class="ti ti-gift"></i>
            </span>
            <span class="hide-menu">Transfer</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="{{url('transaksi/topup')}}" aria-expanded="false">
            <span>
                <i class="ti ti-wallet"></i>
            </span>
            <span class="hide-menu">Topup</span>
            </a>
        </li>
        <li class="sidebar-item">
            <a class="sidebar-link" href="{{url('dashboard/history')}}" aria-expanded="false">
            <span>
                <i class="ti ti-calendar"></i>
            </span>
            <span class="hide-menu">History</span>
            </a>
        </li>
        <li class="nav-small-cap">
            <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
            <span class="hide-menu">AUTH</span>
        </li>
        <li class="sidebar-item">

            <a class="sidebar-link" href="{{ route('logout') }}"
            onclick="event.preventDefault();
            document.getElementById('logout-form').submit();"
            >
                <span>
                    <i class="ti ti-login"></i>
                </span>
                <span class="hide-menu">Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                @csrf
            </form>
        </li>

        </ul>
        <div class="unlimited-access hide-menu bg-light-primary position-relative mb-7 mt-5 rounded">
        <div class="d-flex">
            <div class="unlimited-access-title me-3">
            <h6 class="fw-semibold fs-4 mb-6 text-dark w-85">Official home page</h6>
            <a href="{{url('/')}}" class="btn btn-primary fs-2 fw-semibold lh-sm">Home page</a>
            </div>
            <div class="unlimited-access-img">
            <img src="{{asset('assets/assets/images/backgrounds/rocket.png')}}" alt="" class="img-fluid">
            </div>
        </div>
        </div>
    </nav>
    <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!--  Sidebar End -->
