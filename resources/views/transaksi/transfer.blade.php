@extends('templates.layouts')

@section('content')
<div class="container-fluid">
    <div class="card">
    <div class="card-body">
        <h5 class="card-title fw-semibold mb-4">Forms Transfer</h5>
        <div class="card">
        <div class="card-body">
            <form action="{{ route('transferStore') }}" method="post" role="form">
                @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Transfer To</label>
                <select name="transfer_to" class="form-control">
                    <option value="" selected disabled>Pilih user</option>
                    @foreach($users as $u)
                        <option value="{{$u->id}}">{{$u->name}}</option>
                    @endforeach
                </select>
                <div id="emailHelp" class="form-text">Pastikan user yang anda pilih benar</div>
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Amount</label>
                <input name="amount" type="number" class="form-control" id="exampleInputPassword1">
            </div>
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Keterangan</label>
                <textarea name="keterangan" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection
