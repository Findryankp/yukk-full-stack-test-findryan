@extends('templates.layouts')

@section('content')
<div class="container-fluid">
    <div class="card">
    <div class="card-body">
        <h5 class="card-title fw-semibold mb-4">Forms Topup</h5>
        <div class="card">
        <div class="card-body">
            <form action="{{ route('topupStore') }}" method="post" role="form" enctype="multipart/form-data">
                @csrf
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Amount</label>
                    <input name="amount" type="number" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Keterangan</label>
                    <textarea name="keterangan" class="form-control"></textarea>
                </div>

                <div class="mb-3">
                    <label for="file1" class="form-label">Bukti Upload</label>
                    <input name="file" type="file" class="form-control" id="file1">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection
