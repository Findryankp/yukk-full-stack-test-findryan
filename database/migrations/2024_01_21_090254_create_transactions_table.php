<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->enum('type', ['topup', 'transferTo','transferFrom']);
            $table->decimal('total', 10, 2);
            $table->decimal('saldo_before', 10, 2);
            $table->decimal('saldo_after', 10, 2);
            $table->string('via');
            $table->string('status');
            $table->foreignId('transfer_to')->nullable()->constrained('users');
            $table->string('transaction_code');
            $table->string('file')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('transactions');
    }
};
